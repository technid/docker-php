FROM php:7.1-fpm

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get -y install libfontconfig zip libfreetype6 libmagickwand-dev libmagickcore-dev
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer
RUN composer global require hirak/prestissimo
RUN docker-php-ext-install pdo_mysql bz2
RUN pecl install imagick && docker-php-ext-enable imagick
